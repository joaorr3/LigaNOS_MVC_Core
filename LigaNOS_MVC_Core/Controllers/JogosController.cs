﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using LigaNOS_MVC_Core.Data;
using LigaNOS_MVC_Core.Helpers;
using LigaNOS_MVC_Core.Models;
using LigaNOS_MVC_Core.ViewModels;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;

namespace LigaNOS_MVC_Core.Controllers
{
    public class JogosController : Controller
    {
        private readonly LigaNosDbContext _context;
        private readonly UserManager<ApplicationUser> _userManager;
        private readonly SignInManager<ApplicationUser> _signInManager;


        public JogosController(
            LigaNosDbContext context,
            UserManager<ApplicationUser> userManager,
            SignInManager<ApplicationUser> signInManager)
        {
            _context = context;
            _userManager = userManager;
            _signInManager = signInManager;
        }


        public IActionResult Index()
        {

            var jogos =
                (from a in _context.Jogo
                 join b in _context.Equipa on a.idHomeEquipa equals b.IdEquipa
                 join c in _context.Equipa on a.idAwayEquipa equals c.IdEquipa
                 select new
                 {
                     a.idJogo,
                     a.idLiga,
                     homeEquipa = b.Nome,
                     a.goalsHomeTeam,
                     a.goalsAwayTeam,
                     awayEquipa = c.Nome,
                     a.Data
                 }
                ).Distinct().ToList();



            var jogoViewModel = new JogosViewModel()
            {
                ListJogos = new List<Jogo>(),
                ListLiga = new List<Liga>(),
                ListJogosNomeEquipaModels = new List<JogosNomeEquipaModel>()

            };

            var userLiga = _context.Liga.Where(x => x.idUser == _userManager.GetUserId(User));

            var ListaJogosIdLiga = new List<Jogo>();

            //var ListaJogosNome = new List<JogosNomeEquipaModel>();

            foreach (var item in userLiga)
            {

                //var listJogos = _context.Jogo.Where(x => x.idLiga == item.idLiga).ToList();

                var listJogos = jogos.Where(x => x.idLiga == item.idLiga).ToList();

                foreach (var jogo in listJogos)
                {

                    var ListaJogosNome = new JogosNomeEquipaModel
                    {
                        idJogo = jogo.idJogo,
                        idLiga = jogo.idLiga,
                        homeEquipa = jogo.homeEquipa,
                        goalsHomeTeam = jogo.goalsHomeTeam,
                        goalsAwayTeam = jogo.goalsAwayTeam,
                        awayEquipa = jogo.awayEquipa,
                        Data = jogo.Data
                    };

                    jogoViewModel.ListJogosNomeEquipaModels.Add(ListaJogosNome);
                }

                //foreach (var jogo in listJogos)
                //{
                    
                //    ListaJogosIdLiga.Add(jogo); 
                //}

                jogoViewModel.ListLiga.Add(item);
            }


            //foreach (var item in ListaJogosIdLiga)
            //{
            //    var jogo = new Jogo()
            //    {
            //        idJogo = item.idJogo,
            //        idLiga = item.idLiga,
            //        Data = item.Data,
            //        HomeEquipa = item.HomeEquipa,
            //        AwayEquipa = item.AwayEquipa,
            //        goalsHomeTeam = item.goalsHomeTeam,
            //        goalsAwayTeam = item.goalsAwayTeam
            //    };


            //    jogoViewModel.ListJogos.Add(jogo);
            //}

            return View(jogoViewModel);

        }

        [HttpGet]
        public IActionResult DetalhesLiga(int id)
        {

            var jogos =
                (from a in _context.Jogo
                    join b in _context.Equipa on a.idHomeEquipa equals b.IdEquipa
                    join c in _context.Equipa on a.idAwayEquipa equals c.IdEquipa
                    select new
                    {
                        a.idJogo,
                        a.idLiga,
                        homeEquipa = b.Nome,
                        a.goalsHomeTeam,
                        a.goalsAwayTeam,
                        awayEquipa = c.Nome,
                        a.Data
                    }
                ).Distinct().ToList();


            ComboHelpers combosHelper = new ComboHelpers(_context);

            ViewBag.NomeEquipas = new SelectList(combosHelper.GetEquipas(), "IdEquipa", "Nome");

            var jogosLiga = _context.Jogo.Where(x => x.idLiga == id);
            var listJogos = jogos.Where(x => x.idLiga == id).ToList();

            var jogoViewModel = new JogosViewModel()
            {
                ListJogos = new List<Jogo>(),
                Liga = new Liga(),
                ListJogosNomeEquipaModels = new List<JogosNomeEquipaModel>()

            };

            jogoViewModel.Liga.Nome = _context.Liga.Find(id).Nome;

            foreach (var jogo in listJogos)
            {

                var ListaJogosNome = new JogosNomeEquipaModel
                {
                    idJogo = jogo.idJogo,
                    idLiga = jogo.idLiga,
                    homeEquipa = jogo.homeEquipa,
                    goalsHomeTeam = jogo.goalsHomeTeam,
                    goalsAwayTeam = jogo.goalsAwayTeam,
                    awayEquipa = jogo.awayEquipa,
                    Data = jogo.Data
                };

                jogoViewModel.ListJogosNomeEquipaModels.Add(ListaJogosNome);
            }


            foreach (var item in jogosLiga)
            {
                var jogo = new Jogo()
                {
                    idJogo = item.idJogo,
                    idLiga = item.idLiga,
                    Data = item.Data,
                    HomeEquipa = item.HomeEquipa,
                    AwayEquipa = item.AwayEquipa,
                    goalsHomeTeam = item.goalsHomeTeam,
                    goalsAwayTeam = item.goalsAwayTeam
                };


                jogoViewModel.ListJogos.Add(jogo);
            }

            return View(jogoViewModel);

        }

        [HttpPost]
        public IActionResult InserirJogo(JogosViewModel model)
        {

            try
            {
                foreach (var jogo in model.ListJogos)
                {

                    var _jogo = _context.Jogo.Find(jogo.idJogo);

                    //var _jogo = _context.Jogo.FirstOrDefault(x => x.idLiga == jogo.idLiga);

                    if (jogo.idHomeEquipa != null && jogo.idAwayEquipa != null)
                    {
                        _jogo.idHomeEquipa = jogo.idHomeEquipa;
                        _jogo.idAwayEquipa = jogo.idAwayEquipa;
                        _jogo.Data = jogo.Data;
                    }

                    if (jogo.goalsHomeTeam != 0 && jogo.goalsAwayTeam != 0)
                    {
                        _jogo.goalsHomeTeam = jogo.goalsHomeTeam;
                        _jogo.goalsAwayTeam = jogo.goalsAwayTeam;
                        _jogo.status = true;
                    }

                    if (jogo.Data.ToString() != "0001-01-01 00:00:00.0000000")
                    {
                        _jogo.Data = jogo.Data;
                    }

                }

                _context.SaveChanges();
            }
            catch (Exception e)
            {
                Console.WriteLine(e);
                throw;
            }

            return RedirectToAction("Index", "Jogos");
        }
    }
}