﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using LigaNOS_MVC_Core.Data;
using LigaNOS_MVC_Core.Models;

namespace LigaNOS_MVC_Core.Controllers
{
    [Produces("application/json")]
    [Route("api/Classificacao")]
    public class ScoreTablesController : Controller
    {
        private readonly LigaNosDbContext _context;

        

        public ScoreTablesController(LigaNosDbContext context)
        {
            _context = context;
          
        }

        //// GET: api/Classificacao
        //[HttpGet]
        //public IEnumerable<ScoreTable> GetClassificacao()
        //{
        //    return _context.Classificacao;
        //}

        // GET: api/Classificacao
        [HttpGet]
        public IQueryable GetClassificacao()
        {
            //var scoreTable = await _context.Classificacao.SingleOrDefaultAsync(m => m.IdScoreTable == id);

            var scoreTable = 
                (from a in _context.Equipa
                 join b in _context.Classificacao on a.IdEquipa 
                        equals b.IdEquipa
                    select new {
                        a.Nome,
                        b.Derrotas,
                        b.Empates,
                        b.GolosMarcados,
                        b.GolosSofridos,
                        b.JogosJogados,
                        b.Pontos,
                        b.Vitorias,
                        b.ImgLink
                    }
                    ).Distinct().OrderByDescending(x => x.Pontos);


            return scoreTable;
        }


        // GET: api/Classificacao/5
        [HttpGet("{id}")]
        public async Task<IActionResult> GetScoreTable([FromRoute] int id)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            var scoreTable = await _context.Classificacao.SingleOrDefaultAsync(m => m.IdScoreTable == id);

            if (scoreTable == null)
            {
                return NotFound();
            }

            return Ok(scoreTable);
        }

        // PUT: api/Classificacao/5
        [HttpPut("{id}")]
        public async Task<IActionResult> PutScoreTable([FromRoute] int id, [FromBody] ScoreTable scoreTable)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            if (id != scoreTable.IdScoreTable)
            {
                return BadRequest();
            }

            _context.Entry(scoreTable).State = EntityState.Modified;

            try
            {
                await _context.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!ScoreTableExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return NoContent();
        }

        // POST: api/Classificacao
        [HttpPost]
        public async Task<IActionResult> PostScoreTable([FromBody] ScoreTable scoreTable)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            _context.Classificacao.Add(scoreTable);
            await _context.SaveChangesAsync();

            return CreatedAtAction("GetScoreTable", new { id = scoreTable.IdScoreTable }, scoreTable);
        }

        // DELETE: api/Classificacao/5
        [HttpDelete("{id}")]
        public async Task<IActionResult> DeleteScoreTable([FromRoute] int id)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            var scoreTable = await _context.Classificacao.SingleOrDefaultAsync(m => m.IdScoreTable == id);
            if (scoreTable == null)
            {
                return NotFound();
            }

            _context.Classificacao.Remove(scoreTable);
            await _context.SaveChangesAsync();

            return Ok(scoreTable);
        }

        private bool ScoreTableExists(int id)
        {
            return _context.Classificacao.Any(e => e.IdScoreTable == id);
        }
    }
}