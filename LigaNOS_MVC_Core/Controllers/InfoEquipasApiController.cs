﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using LigaNOS_MVC_Core.Data;
using LigaNOS_MVC_Core.Models;

namespace LigaNOS_MVC_Core.Controllers
{
    [Produces("application/json")]
    [Route("api/InfoEquipasApi")]
    public class InfoEquipasApiController : Controller
    {
        private readonly LigaNosDbContext _context;

        public InfoEquipasApiController(LigaNosDbContext context)
        {
            _context = context;
        }

        // GET: api/InfoEquipasApi
        [HttpGet]
        public IEnumerable<Equipa> GetInfoEquipas()
        {
            return _context.Equipa;
        }

        // GET: api/InfoEquipasApi/5
        [HttpGet("{id}")]
        public async Task<IActionResult> GetInfoEquipas([FromRoute] int id)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            var infoEquipas = await _context.Equipa.SingleOrDefaultAsync(m => m.IdEquipa == id);

            if (infoEquipas == null)
            {
                return NotFound();
            }

            return Ok(infoEquipas);
        }

        // PUT: api/InfoEquipasApi/5
        [HttpPut("{id}")]
        public async Task<IActionResult> PutInfoEquipas([FromRoute] int id, [FromBody] Equipa infoEquipas)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            if (id != infoEquipas.IdEquipa)
            {
                return BadRequest();
            }

            _context.Entry(infoEquipas).State = EntityState.Modified;

            try
            {
                await _context.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!InfoEquipasExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return NoContent();
        }

        // POST: api/InfoEquipasApi
        [HttpPost]
        public async Task<IActionResult> PostInfoEquipas([FromBody] Equipa infoEquipas)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            _context.Equipa.Add(infoEquipas);
            await _context.SaveChangesAsync();

            return CreatedAtAction("GetInfoEquipas", new { id = infoEquipas.IdEquipa }, infoEquipas);
        }

        // DELETE: api/InfoEquipasApi/5
        [HttpDelete("{id}")]
        public async Task<IActionResult> DeleteInfoEquipas([FromRoute] int id)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            var infoEquipas = await _context.Equipa.SingleOrDefaultAsync(m => m.IdEquipa == id);
            if (infoEquipas == null)
            {
                return NotFound();
            }

            _context.Equipa.Remove(infoEquipas);
            await _context.SaveChangesAsync();

            return Ok(infoEquipas);
        }

        private bool InfoEquipasExists(int id)
        {
            return _context.Equipa.Any(e => e.IdEquipa == id);
        }
    }
}