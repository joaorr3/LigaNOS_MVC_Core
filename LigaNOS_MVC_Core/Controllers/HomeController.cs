﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using LigaNOS_MVC_Core.Data;
using LigaNOS_MVC_Core.Models;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Options;
using LigaNOS_MVC_Core.Models.HomeViewModel;
using LigaNOS_MVC_Core.Models.ManageViewModels;
using LigaNOS_MVC_Core.Models.ViewModels;
using Microsoft.AspNetCore.Identity;

namespace LigaNOS_MVC_Core.Controllers
{
    public class HomeController : Controller
    {
        private readonly LigaNosDbContext _context;

        private readonly UserManager<ApplicationUser> _userManager;
        private readonly SignInManager<ApplicationUser> _signInManager;


        public HomeController(LigaNosDbContext context, UserManager<ApplicationUser> userManager, SignInManager<ApplicationUser> signInManager)
        {
            _context = context;
            _userManager = userManager;
            _signInManager = signInManager;

        }

        public async Task<IActionResult> Index()
        {
            if (_signInManager.IsSignedIn(User))
            {
                var user = await _userManager.GetUserAsync(User);
                if (user == null)
                {
                    //throw new ApplicationException($"Unable to load user with ID '{_userManager.GetUserId(User)}'.");
                    return View();
                }

                var SinglePageViewModel = new SinglePageViewModel
                {
                    IndexViewModel = new IndexViewModel
                    {
                        Username = user.UserName,
                        Email = user.Email,
                        PhoneNumber = user.PhoneNumber,
                        IsEmailConfirmed = user.EmailConfirmed,
                    }
                };

                return View(SinglePageViewModel);
            }

            return View();
        }

        //[HttpGet, ActionName("Index")]
        //public async Task<IActionResult> GetUserData()
        //{
        //    var user = await _userManager.GetUserAsync(User);
        //    if (user == null)
        //    {
        //        throw new ApplicationException($"Unable to load user with ID '{_userManager.GetUserId(User)}'.");
        //    }

        //    var SinglePageViewModel = new SinglePageViewModel
        //    {
        //        IndexViewModel = new IndexViewModel
        //        {
        //            Username = user.UserName,
        //            Email = user.Email,
        //            PhoneNumber = user.PhoneNumber,
        //            IsEmailConfirmed = user.EmailConfirmed,
        //        }
        //    };

        //    return View(SinglePageViewModel);
        //}



        [HttpGet]
        public IActionResult Clubes()
        {

            var _homeViewModel = new HomeViewModel
            {
                InfoEquipas = _context.Equipa
            };

            return View(_homeViewModel);

        }


    }
}
