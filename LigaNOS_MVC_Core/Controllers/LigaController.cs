﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using LigaNOS_MVC_Core.Data;
using LigaNOS_MVC_Core.Helpers;
using LigaNOS_MVC_Core.Models;
using LigaNOS_MVC_Core.ViewModels;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;

namespace LigaNOS_MVC_Core.Controllers
{
    public class LigaController : Controller
    {

        private readonly LigaNosDbContext _context;
        private readonly UserManager<ApplicationUser> _userManager;
        private readonly SignInManager<ApplicationUser> _signInManager;



        public LigaController(
            LigaNosDbContext context, 
            UserManager<ApplicationUser> userManager,
            SignInManager<ApplicationUser> signInManager)
        {
            _context = context;
            _userManager = userManager;
            _signInManager = signInManager;
        }


        public IActionResult Index()
        {

            var userLiga = _context.Liga.Where(x => x.idUser == _userManager.GetUserId(User));

            var ligaEquipasViewModel = new LigaViewModel()
            {
                ListLiga = new List<Liga>()

            };

            foreach (var item in userLiga)
            {

                var liga = new Liga
                {
                    idLiga = item.idLiga,
                    Nome = item.Nome,
                    NumeroDeEquipas = item.NumeroDeEquipas,
                    DataDeInicio = item.DataDeInicio

                };

                ligaEquipasViewModel.ListLiga.Add(liga);
            }


            return View(ligaEquipasViewModel);
        }


        public IActionResult CriarLiga()
        {

            ComboHelpers combosHelper = new ComboHelpers(_context);

            ViewBag.NumeroDeEquipas = new SelectList(combosHelper.GetNumeroDeEquipas());

            return View();
        }

        [HttpPost]
        public IActionResult CreateLiga(LigaViewModel model)
        {
            model.Liga.idUser = _userManager.GetUserId(User);
            model.Liga.DataDeInicio = DateTime.Now;

            var numeroEquipas = model.Liga.NumeroDeEquipas;

            var numeroJornadas = (numeroEquipas - 1) * 2;

            var numeroJogos = numeroJornadas * (numeroEquipas / 2);

            var Jogo = new Jogo();
            var ScoreTable = new ScoreTable();

            try
            {
                _context.Liga.Add(model.Liga);

                for (int i = 0; i < numeroEquipas; i++)
                {
                    ScoreTable = new ScoreTable
                    {
                        idLiga = model.Liga.idLiga,
                    };

                    _context.Classificacao.Add(ScoreTable);
                }

                for (int i = 0; i < numeroJogos; i++)
                {
                    Jogo = new Jogo
                    {
                        idLiga = model.Liga.idLiga,
                        idHomeEquipa = 0,
                        idAwayEquipa = 0,
                        status = false
                    };

                    _context.Jogo.Add(Jogo);
                }

                _context.SaveChanges();
            }
            catch (Exception e)
            {
                Console.WriteLine(e);
                throw;
            }

            return RedirectToAction("Index", "Liga");

        }



        public IActionResult CriarLigaAuto()
        {

            ComboHelpers combosHelper = new ComboHelpers(_context);

            ViewBag.NumeroDeEquipas = new SelectList(combosHelper.GetNumeroDeEquipas());

            return View();
        }

        public IActionResult CriarLigaAutoEscolherEquipas(LigaViewModel model)
        {

            ComboHelpers combosHelper = new ComboHelpers(_context);
            ViewBag.NomeEquipas = new SelectList(combosHelper.GetEquipas(), "IdEquipa", "Nome");

            return View(model);
        }

        public IActionResult CriarLigaAutoConfirmacao(LigaViewModel model)
        {

            HttpContext.Session.SetObjectAsJson("LigaViewModel", model);

            return View(model);
        }

        [HttpPost]
        public IActionResult CreateLigaAuto(LigaViewModel model)
        {
            var ListJogos1 = new List<Jogo>();

            var ligaViewModel = HttpContext.Session.GetObjectFromJson<LigaViewModel>("LigaViewModel");

            var listEquipas = new List<int>();

            foreach (var item in ligaViewModel.ListEquipas)
            {
                listEquipas.Add(item.IdEquipa);
            }

            model.Liga.idUser = _userManager.GetUserId(User);
            model.Liga.DataDeInicio = DateTime.Now;

            _context.Liga.Add(model.Liga);

            #region Auto Jogos


            List<Jogo> jogos = CalculateFixtures(listEquipas);


            #endregion


            var Jogo = new Jogo();

            var ScoreTable = new ScoreTable();

            foreach (var item in listEquipas)
            {
                ScoreTable = new ScoreTable
                {

                    idLiga = model.Liga.idLiga,
                    IdEquipa = item
                    
                };

                _context.Classificacao.Add(ScoreTable);
            }


            foreach (var item in jogos)
            {
                Jogo = new Jogo
                {
                    idLiga = model.Liga.idLiga,
                    idHomeEquipa = item.idHomeEquipa,
                    idAwayEquipa = item.idAwayEquipa,
                    status = false
                };

                _context.Jogo.Add(Jogo);
            }

            try
            {

                _context.SaveChanges();

            }
            catch (Exception e)
            {
                Console.WriteLine(e);
                throw;
            }

            return RedirectToAction("Index", "Liga");

        }


        [HttpGet]
        public IActionResult DeleteLiga(int id)
        {
            var liga = _context.Liga.SingleOrDefault(m => m.idLiga == id);

            var jogosDaLiga = _context.Jogo.Where(m => m.idLiga == id);

            try
            {
                if (liga != null)
                {
                    _context.Liga.Remove(liga);

                    foreach (var item in jogosDaLiga)
                    {
                        _context.Jogo.Remove(item);
                    }

                    _context.SaveChanges();
                }

            }
            catch (Exception e)
            {
                Console.WriteLine(e);
                throw;
            }


            return RedirectToAction("Index", "Liga");
        }

        #region Helpers

        List<Jogo> CalculateFixtures(List<int> players)
        {
            //create a list of all possible Jogos (order not important)
            List<Jogo> Jogos = new List<Jogo>();
            for (int i = 0; i < players.Count; i++)
            {
                for (int j = 0; j < players.Count; j++)
                {
                    if (players[i] != players[j])
                    {
                        Jogos.Add(new Jogo() { idHomeEquipa = players[i], idAwayEquipa = players[j] });
                    }
                }
            }

            Jogos.Reverse();//reverse the Jogo list as we are going to remove element from this and will therefore have to start at the end

            //var numeroEquipas = ligaViewModel.Liga.NumeroDeEquipas;

            //var numeroJornadas = (numeroEquipas - 1) * 2;
            //var numeroJogos = numeroJornadas * (numeroEquipas / 2);

            //calculate the number of game weeks and the number of games per week
            int gameweeks = (players.Count - 1) * 2;
            int gamesPerWeek = gameweeks / 2;

            List<Jogo> sortedJogos = new List<Jogo>();

            //foreach game week get all available Jogo for that week and add to sorted list
            for (int i = 0; i < gameweeks; i++)
            {
                sortedJogos.AddRange(TakeUnique(Jogos, gamesPerWeek));
            }

            return sortedJogos;

        }

        List<Jogo> TakeUnique(List<Jogo> Jogos, int gamesPerWeek)
        {
            List<Jogo> result = new List<Jogo>();

            //pull enough Jogo to cater for the number of game to play
            for (int i = 0; i < gamesPerWeek; i++)
            {
                //loop all Jogo to find an unused set of teams
                for (int j = Jogos.Count - 1; j >= 0; j--)
                {
                    //check to see if any teams in current fixtue have already been used this game week and ignore if they have
                    if (!result.Any(r => r.idHomeEquipa == Jogos[j].idHomeEquipa || r.idAwayEquipa == Jogos[j].idHomeEquipa || r.idHomeEquipa == Jogos[j].idAwayEquipa || r.idAwayEquipa == Jogos[j].idAwayEquipa))
                    {
                        //teams not yet used
                        result.Add(Jogos[j]);
                        Jogos.RemoveAt(j);
                    }
                }
            }

            return result;
        }


        #endregion
    }
}