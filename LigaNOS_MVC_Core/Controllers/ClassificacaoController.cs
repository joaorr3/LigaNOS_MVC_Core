﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using LigaNOS_MVC_Core.Data;
using LigaNOS_MVC_Core.Models;
using LigaNOS_MVC_Core.ViewModels;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;

namespace LigaNOS_MVC_Core.Controllers
{
    public class ClassificacaoController : Controller
    {
        private readonly LigaNosDbContext _context;
        private readonly UserManager<ApplicationUser> _userManager;
        private readonly SignInManager<ApplicationUser> _signInManager;

        public ClassificacaoController(
            LigaNosDbContext context,
            UserManager<ApplicationUser> userManager,
            SignInManager<ApplicationUser> signInManager)
        {
            _context = context;
            _userManager = userManager;
            _signInManager = signInManager;
        }


        public IActionResult Index()
        {
            var jogos =
                (from a in _context.Jogo
                 join b in _context.Equipa on a.idHomeEquipa equals b.IdEquipa
                 join c in _context.Equipa on a.idAwayEquipa equals c.IdEquipa
                 select new
                 {
                     a.idJogo,
                     a.idLiga,
                     homeEquipa = b.Nome,
                     a.goalsHomeTeam,
                     a.goalsAwayTeam,
                     awayEquipa = c.Nome,
                     a.Data,
                     a.status
                 }
                ).Distinct().ToList();


            #region Lista de nomes das equipas na liga do user logado

            //var LigaUser = _context.Liga.Where(x => x.idUser == _userManager.GetUserId(User));

            //var todosOsNomes = new Dictionary<int, string>();

            //foreach (var liga in LigaUser)
            //{
            //    var listJogos = jogos.Where(x => x.idLiga == liga.idLiga).ToList();

            //    foreach (var item in listJogos)
            //    {
            //        todosOsNomes.Add(liga.idLiga, item.homeEquipa);
            //        todosOsNomes.Add(liga.idLiga, item.awayEquipa);
            //    }

            //    var NomesLiga = (from a in todosOsNomes
            //                     where a.Key == liga.idLiga
            //                     select new
            //                     {
            //                         a.Value,
            //                         a.Key
            //                     }
            //        ).Distinct().ToList();
            //}

            #endregion

            #region Comment

            //var todosOsNomes = new List<EquipasNaLiga>();

            //foreach (var liga in LigaUser)
            //{
            //    var listJogos = jogos.Where(x => x.idLiga == liga.idLiga).ToList();

            //foreach (var item in listJogos)
            //{
            //    todosOsNomes.Add(new EquipasNaLiga
            //    {
            //        IdLiga = liga.idLiga,
            //        Nome = item.homeEquipa
            //    });

            //    todosOsNomes.Add(new EquipasNaLiga
            //    {
            //        IdLiga = liga.idLiga,
            //        Nome = item.awayEquipa
            //    });
            //}
            //}


            #endregion

            var todosOsNomes = new List<EquipasNaLiga>();

            var classificacaoViewModel = new ClassificacaoViewModel()
            {
                ListScoreTableNomesEquipasModels = new List<ScoreTableNomesEquipasModel>(),
                ListLiga = new List<Liga>()
            };

            var userLiga = _context.Liga.Where(x => x.idUser == _userManager.GetUserId(User));

            //var ListaJogosIdLiga = new List<Jogo>();

            //var ListaJogosNome = new List<JogosNomeEquipaModel>();

            foreach (var liga in userLiga)
            {

                var listJogos = jogos.Where(x => x.idLiga == liga.idLiga).ToList();

                classificacaoViewModel.ListLiga.Add(liga);

                foreach (var item in listJogos)
                {
                    todosOsNomes.Add(new EquipasNaLiga
                    {
                        IdLiga = liga.idLiga,
                        Nome = item.homeEquipa
                    });

                    todosOsNomes.Add(new EquipasNaLiga
                    {
                        IdLiga = liga.idLiga,
                        Nome = item.awayEquipa
                    });
                }

                var NomesLiga = (from a in todosOsNomes
                                 where a.IdLiga == liga.idLiga
                                 select new
                                 {
                                     a.Nome,
                                     a.IdLiga
                                 }
                    ).Distinct().ToList();

                foreach (var nome in NomesLiga)
                {
                    int vitorias = 0, empates = 0, derrotas = 0, golosMarcados = 0, golosSofridos = 0, pontos = 0, jogosJogados = 0;

                    foreach (var item in listJogos)
                    {
                        if (item.status != false)
                        {
                            if (nome.Nome == item.homeEquipa)
                            {
                                golosMarcados += item.goalsHomeTeam;
                                golosSofridos += item.goalsAwayTeam;
                                jogosJogados += 1;

                                if (item.goalsHomeTeam > item.goalsAwayTeam)
                                {
                                    pontos += 3;
                                    vitorias += 1;
                                }
                                else if (item.goalsHomeTeam < item.goalsAwayTeam)
                                {
                                    derrotas += 1;
                                }
                                else if (item.goalsHomeTeam == item.goalsAwayTeam)
                                {
                                    pontos += 1;
                                    empates += 1;
                                }
                            }

                            if (nome.Nome == item.awayEquipa)
                            {
                                golosMarcados += item.goalsAwayTeam;
                                golosSofridos += item.goalsHomeTeam;
                                jogosJogados += 1;

                                if (item.goalsAwayTeam > item.goalsHomeTeam)
                                {
                                    pontos += 3;
                                    vitorias += 1;
                                }
                                else if (item.goalsAwayTeam < item.goalsHomeTeam)
                                {
                                    derrotas += 1;
                                }
                                else if (item.goalsAwayTeam == item.goalsHomeTeam)
                                {
                                    pontos += 1;
                                    empates += 1;
                                }
                            }
                        }
                        
                    }

                    var scoreTableNomesEquipasModel = new ScoreTableNomesEquipasModel
                    {
                        idLiga = nome.IdLiga,
                        NomeEquipa = nome.Nome,
                        JogosJogados = jogosJogados,
                        Vitorias = vitorias,
                        Empates = empates,
                        Derrotas = derrotas,
                        GolosMarcados = golosMarcados,
                        GolosSofridos = golosSofridos,
                        Pontos = pontos,
                    };

                    classificacaoViewModel.ListScoreTableNomesEquipasModels.Add(scoreTableNomesEquipasModel);
                }
            }

            var sortedList = classificacaoViewModel.ListScoreTableNomesEquipasModels.OrderByDescending(x => x.Pontos).ToList();

            classificacaoViewModel.ListScoreTableNomesEquipasModels = sortedList;

            return View(classificacaoViewModel);
        }
    }
}