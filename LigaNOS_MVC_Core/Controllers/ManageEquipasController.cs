﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using LigaNOS_MVC_Core.Data;
using LigaNOS_MVC_Core.Models;
using LigaNOS_MVC_Core.ViewModels;
using Microsoft.AspNetCore.Mvc;

namespace LigaNOS_MVC_Core.Controllers
{
    public class ManageEquipasController : Controller
    {

        private readonly LigaNosDbContext _context;

        public ManageEquipasController(LigaNosDbContext context)
        {
            _context = context;
        }


        [HttpGet]
        public IActionResult Index()
        {
            var manageEquipasViewModel = new ManageEquipasViewModel
            {
                ListEquipas = new List<Equipa>()
                
            };

            foreach (var item in _context.Equipa)
            {

                var equipa = new Equipa
                {
                    IdEquipa = item.IdEquipa,
                    Nome = item.Nome,
                    Estadio = item.Estadio,
                    Local = item.Local,
                    NomeTreinador = item.NomeTreinador,
                    NomePresidente = item.NomePresidente,
                    Patrocinador = item.Patrocinador,
                    Plantel = item.Plantel,
                }; 
                
                manageEquipasViewModel.ListEquipas.Add(equipa);

            }

            var sortedList = manageEquipasViewModel.ListEquipas.OrderByDescending(x => x.IdEquipa).ToList();

            manageEquipasViewModel.ListEquipas = sortedList;

            return View(manageEquipasViewModel);
        }


        [HttpPost]
        public IActionResult NewTeam(ManageEquipasViewModel model)
        {
            try
            {
                _context.Equipa.Add(model.Equipa);

                _context.SaveChanges();
            }
            catch (Exception e)
            {
                Console.WriteLine(e);
                throw;
            }
            

            return RedirectToAction("Index", "ManageEquipas");
        }

        [HttpGet]
        public IActionResult DeleteTeam(int id)
        {
            var equipa = _context.Equipa.SingleOrDefault(m => m.IdEquipa == id);

            try
            {
                if (equipa != null)
                {
                    _context.Equipa.Remove(equipa);

                    _context.SaveChanges();
                }

            }
            catch (Exception e)
            {
                Console.WriteLine(e);
                throw;
            }


            return RedirectToAction("Index", "ManageEquipas");
        }

        [HttpGet]
        public IActionResult UpdateTeam(int id)
        {
            var equipaFound = _context.Equipa.SingleOrDefault(m => m.IdEquipa == id);

            var manageEquipasViewModel = new ManageEquipasViewModel
            {
                Equipa = equipaFound

            };

            return View(manageEquipasViewModel);
        }

        [HttpPost]
        public IActionResult UpdateTeamPost(ManageEquipasViewModel updateEquipa)
        {

            var equipaFound = _context.Equipa.Find(updateEquipa.Equipa.IdEquipa);

            equipaFound.Nome = updateEquipa.Equipa.Nome;
            equipaFound.Estadio = updateEquipa.Equipa.Estadio;
            equipaFound.Local = updateEquipa.Equipa.Local;
            equipaFound.NomePresidente = updateEquipa.Equipa.NomePresidente;
            equipaFound.NomeTreinador = updateEquipa.Equipa.NomeTreinador;
            equipaFound.Patrocinador = updateEquipa.Equipa.Patrocinador;
            equipaFound.Plantel = updateEquipa.Equipa.Plantel;


            try
            {
                

                _context.SaveChanges();

            }
            catch (Exception e)
            {
                Console.WriteLine(e);
                throw;
            }


            return RedirectToAction("Index", "ManageEquipas");
        }


    }
}