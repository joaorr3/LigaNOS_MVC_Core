﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Identity.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore;
using LigaNOS_MVC_Core.Models;
using Microsoft.CodeAnalysis.Differencing;

namespace LigaNOS_MVC_Core.Data
{
    public class LigaNosDbContext : IdentityDbContext<ApplicationUser>
    {
        public LigaNosDbContext(DbContextOptions<LigaNosDbContext> options) : base(options)
        {
        }

        protected override void OnModelCreating(ModelBuilder builder)
        {
            base.OnModelCreating(builder);
            // Customize the ASP.NET Identity model and override the defaults if needed.
            // For example, you can rename the ASP.NET Identity table names and more.
            // Add your customizations after calling base.OnModelCreating(builder);

        }

        public DbSet<Equipa> Equipa { get; set; }
        public DbSet<ScoreTable> Classificacao { get; set; }
        public DbSet<Jogo> Jogo { get; set; }
        public DbSet<Liga> Liga { get; set; }



    }
}
