﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using LigaNOS_MVC_Core.Models;
using Microsoft.AspNetCore.Identity;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;

namespace LigaNOS_MVC_Core.Mvc.Roles
{
    public class CreateRolesSeed
    {

        public static async Task CreateRoles(IServiceProvider serviceProvider, IConfiguration Configuration)
        {
            //adding customs roles
            var RoleManager = serviceProvider.GetRequiredService<RoleManager<IdentityRole>>();
            var UserManager = serviceProvider.GetRequiredService<UserManager<ApplicationUser>>();
            string[] roleNames = { "SuperAdmin", "User" };
            IdentityResult roleResult;

            foreach (var roleName in roleNames)
            {
                // creating the roles and seeding them to the database
                var roleExist = await RoleManager.RoleExistsAsync(roleName);
                if (!roleExist)
                {
                    roleResult = await RoleManager.CreateAsync(new IdentityRole(roleName));
                }
            }

            var SuperAdmin = new ApplicationUser
            {
                UserName = Configuration.GetSection("SuperAdminSettings")["UserEmail"],
                Email = Configuration.GetSection("SuperAdminSettings")["UserEmail"]
            };

            string userPassword = Configuration.GetSection("SuperAdminSettings")["UserPassword"];
            var user = await UserManager.FindByEmailAsync(Configuration.GetSection("SuperAdminSettings")["UserEmail"]);

            if (user == null)
            {
                var createSuperAdmin = await UserManager.CreateAsync(SuperAdmin, userPassword);
                if (createSuperAdmin.Succeeded)
                {
                    await UserManager.AddToRoleAsync(SuperAdmin, "SuperAdmin");
                }
            }
        }
    }
}

