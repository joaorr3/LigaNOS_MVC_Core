﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace LigaNOS_MVC_Core.ViewModels
{
    public class EquipasNaLiga
    {

        public string Nome { get; set; }

        public int IdLiga { get; set; }

    }
}
