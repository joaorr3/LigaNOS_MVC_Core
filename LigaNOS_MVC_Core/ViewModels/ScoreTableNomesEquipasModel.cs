﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace LigaNOS_MVC_Core.ViewModels
{
    public class ScoreTableNomesEquipasModel
    {
        public int IdScoreTable { get; set; }
        public int IdEquipa { get; set; }
        public int idLiga { get; set; }
        public string NomeEquipa { get; set; }
        public int JogosJogados { get; set; }
        public int Vitorias { get; set; }
        public int Empates { get; set; }
        public int Derrotas { get; set; }
        public int GolosMarcados { get; set; }
        public int GolosSofridos { get; set; }
        public int Pontos { get; set; }


    }
}
