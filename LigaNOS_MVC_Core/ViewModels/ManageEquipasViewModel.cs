﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using LigaNOS_MVC_Core.Models;

namespace LigaNOS_MVC_Core.ViewModels
{
    public class ManageEquipasViewModel
    {

        public Equipa Equipa { get; set; }

        public List<Equipa> ListEquipas { get; set; }


    }
}
