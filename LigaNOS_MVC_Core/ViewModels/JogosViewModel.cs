﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using LigaNOS_MVC_Core.Models;

namespace LigaNOS_MVC_Core.ViewModels
{
    public class JogosViewModel
    {

        public Liga Liga { get; set; }

        public List<Liga> ListLiga { get; set; }

        public List<Equipa> ListEquipas { get; set; }


        public Jogo Jogo { get; set; }

        public List<Jogo> ListJogos { get; set; }

        public List<JogosNomeEquipaModel> ListJogosNomeEquipaModels { get; set; }

    }
}
