﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using LigaNOS_MVC_Core.Models;

namespace LigaNOS_MVC_Core.ViewModels
{
    public class LigaViewModel
    {

        public Liga Liga { get; set; }

        public ScoreTable ScoreTable { get; set; }


        public List<ScoreTable> ListsScoreTable { get; set; }

        public List<Liga> ListLiga { get; set; }

        public List<Equipa> ListEquipas { get; set; }


    }
}
