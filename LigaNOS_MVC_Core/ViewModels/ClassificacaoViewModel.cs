﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using LigaNOS_MVC_Core.Models;

namespace LigaNOS_MVC_Core.ViewModels
{
    public class ClassificacaoViewModel
    {
        public Liga Liga { get; set; }

        public Jogo Jogo { get; set; }

        public ScoreTable ScoreTable { get; set; }

        public ScoreTableNomesEquipasModel ScoreTableNomesEquipasModels { get; set; }


        public List<ScoreTableNomesEquipasModel> ListScoreTableNomesEquipasModels { get; set; }

        public List<ScoreTable> ListScoreTables { get; set; }

        public List<Liga> ListLiga { get; set; }

        public List<Equipa> ListEquipas { get; set; }

        public List<Jogo> ListJogos { get; set; }

        public List<JogosNomeEquipaModel> ListJogosNomeEquipaModels { get; set; }


    }
}
