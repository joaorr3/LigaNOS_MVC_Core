﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using LigaNOS_MVC_Core.Models;

namespace LigaNOS_MVC_Core.ViewModels
{
    public class JogosNomeEquipaModel
    {

        public int idJogo { get; set; }

        public int idLiga { get; set; }

        public string homeEquipa { get; set; }

        public int goalsHomeTeam { get; set; }

        public int goalsAwayTeam { get; set; }

        public string awayEquipa { get; set; }

        public DateTime Data { get; set; }
    }
}
