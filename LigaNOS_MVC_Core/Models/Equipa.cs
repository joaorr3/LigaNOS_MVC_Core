﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace LigaNOS_MVC_Core.Models
{
    public class Equipa
    {
        [Key, DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int IdEquipa { get; set; }
        public string Nome { get; set; }
        public string Local { get; set; }
        public string Estadio { get; set; }
        public string Patrocinador { get; set; }
        public string NomePresidente { get; set; }
        public string NomeTreinador { get; set; }
        public int Plantel { get; set; }

    }
}
