﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace LigaNOS_MVC_Core.Models
{
    public class Jogo
    {

        [Key, DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int idJogo { get; set; }
        public int idLiga { get; set; }
        public DateTime Data { get; set; }

        public int goalsHomeTeam { get; set; }
        public int goalsAwayTeam { get; set; }

        public bool status { get; set; }


        [ForeignKey("HomeEquipa")]
        public int? idHomeEquipa { get; set; }

        [ForeignKey("AwayEquipa")]
        public int? idAwayEquipa { get; set; }

        [ForeignKey("idLiga")]
        public virtual Liga Liga { get; set; }


        public virtual Equipa HomeEquipa { get; set; }

        public virtual Equipa AwayEquipa { get; set; }


    }
}
