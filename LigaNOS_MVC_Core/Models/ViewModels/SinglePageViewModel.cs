﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using LigaNOS_MVC_Core.Models.AccountViewModels;
using LigaNOS_MVC_Core.Models.ManageViewModels;

namespace LigaNOS_MVC_Core.Models.ViewModels
{
    public class SinglePageViewModel
    {

        public LoginViewModel LoginViewModel { get; set; }

        public IndexViewModel IndexViewModel { get; set; }

        public RegisterViewModel RegisterViewModel { get; set; }
    }
}
