﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace LigaNOS_MVC_Core.Models
{
    public class Liga
    {
        [Key, DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int idLiga { get; set; }
        public string idUser { get; set; }
        public string Nome { get; set; }
        public int NumeroDeEquipas { get; set; }
        public DateTime DataDeInicio { get; set; }
        public virtual ICollection<Jogo> Jogos { get; set; }


        [ForeignKey("idUser")]
        public virtual ApplicationUser ApplicationUser { get; set; }

    }
}
