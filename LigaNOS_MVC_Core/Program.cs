﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using LigaNOS_MVC_Core.Mvc.Roles;
using Microsoft.AspNetCore;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Logging;

namespace LigaNOS_MVC_Core
{
    public class Program
    {
        public static void Main(string[] args)
        {
            BuildWebHost(args).Run();

            //var host = BuildWebHost(args);

            //using (var scope = host.Services.CreateScope())
            //{

            //    var services = scope.ServiceProvider;
            //    try
            //    {

            //        var serviceProvider = services.GetRequiredService<IServiceProvider>();
            //        var configuration = services.GetRequiredService<IConfiguration>();
            //        CreateRolesSeed.CreateRoles(serviceProvider, configuration).Wait();

            //    }
            //    catch (Exception e)
            //    {
            //        var logger = services.GetRequiredService<ILogger<Program>>();
            //        logger.LogError(e, "An error occurred while creating roles");

            //    }


            //}


            //host.Run();
        }

        public static IWebHost BuildWebHost(string[] args) =>
            WebHost.CreateDefaultBuilder(args)
                .UseStartup<Startup>()
                .UseKestrel()
                .UseUrls("http://*:50026")
                .Build();
    }
}
