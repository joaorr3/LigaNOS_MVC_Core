﻿//General FX's
$(document).ready(function() {


    $(window).on('load',function(){
        $('#UpdateTeamModal').modal('show');
    });


    $("tr td input").hover(function () {

            $("#addNewTeamIcon").show();
    });

    $("tr td input").keyup(function () {
        if($("#InputNome").val() === '' && 
            $("#InputEstadio").val() === '' && 
            $("#InputLocal").val() === '' && 
            $("#InputPresidente").val() === '' && 
            $("#InputTreinador").val() === '' && 
            $("#InputPatrocinador").val() === '' && 
            $("#InputPlantel").val() === ''){

            $("#addNewTeamIcon").hide();
        }
    });

    $("tr td input").mouseleave(function () {
        
        if($("#InputNome").val() === '' && 
            $("#InputEstadio").val() === '' && 
            $("#InputLocal").val() === '' && 
            $("#InputPresidente").val() === '' && 
            $("#InputTreinador").val() === '' && 
            $("#InputPatrocinador").val() === '' && 
            $("#InputPlantel").val() === ''){

            $("#addNewTeamIcon").hide();
        }
    });
    
    


    //Paralax

    $(".tabelaEquipas, [data-paroller-factor]").paroller({
        factor: 0.3,
        type: 'foreground',
        direction: 'vertical'
    });



    // Smooth Scrolling

    $(function() {
        $('a[href*="#"]:not([href="#"])').click(function() {
            if (location.pathname.replace(/^\//, '') === this.pathname.replace(/^\//, '') &&
                location.hostname === this.hostname) {
                var target = $(this.hash);
                target = target.length ? target : $('[name=' + this.hash.slice(1) + ']');
                if (target.length) {
                    $('html, body').animate({
                            scrollTop: target.offset().top
                        },
                        800);
                    return false;
                }
            }
        });
    });




});


