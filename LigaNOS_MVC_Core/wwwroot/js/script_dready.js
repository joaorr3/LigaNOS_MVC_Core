﻿//General FX's
$(document).ready(function() {


    $(".modalClick").on("click", function() {
        $('.ui.modal')
            .modal({
                centered: false
            })
            .modal('show');
    });

    //Scroll Reveal

    window.sr = ScrollReveal({
        reset: true
    });

    sr.reveal('#screveal', {
        duration: 850,
        opacity: 0,
        scale: 1,
        distance: '0px',
        mobile: false,
        easing: 'ease-in-out'
    });



    //Paralax

    $(".clubes, [data-paroller-factor]").paroller({
        factor: 0.3,
        type: 'foreground',
        direction: 'vertical'
    });
    $(".classificacao, [data-paroller-factor]").paroller({
        factor: 0.3,
        type: 'foreground',
        direction: 'vertical'
    });



    // Smooth Scrolling

    $(function() {
        $('a[href*="#"]:not([href="#"])').click(function() {
            if (location.pathname.replace(/^\//, '') === this.pathname.replace(/^\//, '') &&
                location.hostname === this.hostname) {
                var target = $(this.hash);
                target = target.length ? target : $('[name=' + this.hash.slice(1) + ']');
                if (target.length) {
                    $('html, body').animate({
                            scrollTop: target.offset().top
                        },
                        800);
                    return false;
                }
            }
        });
    });



    //Initialize Bootstrap Tooltip

    $('[data-toggle="tooltip"]').tooltip();


    //Collapse Mobile Navbar

    $('.navbar-nav>li>a').on('click', function() {
        $('.navbar-collapse').collapse('hide');
    });

    $('body').on('click', function() {
        $('.navbar-collapse').collapse('hide');
    });

});

//Clubs AddClass For FX'x
$(document).ready(function() {

    $(".clubes img").addClass("target");


    $(".clubes img").hover(function(e) {

            $(e.target).addClass("target");
        },
        function(e) {
            $(e.target).removeClass("target");
        });

    $('#content').hide();


    $('.target').popovers({
        content: $('#content'),
        placement: 'auto',
        html: true,
        animation: true
    });


    $('#content').show();

});


//Clubs FX's
$(document).ready(function() {
    var corNormal = "rgba(255, 230, 0, 0.5)";
    var corHover = "rgba(180, 155, 0, 0.5)";
    var brightness50 = "brightness(70%)";
    var brightness100 = "brightness(100%)";
    var bkgTime = "1s";
    var shadow = "drop-shadow(0 40px 15px rgba(0, 0, 0, 0.3))";
    var time = "0.5s";

    $(".clubes img").hover(function() {
            $(".clubes img").css({ filter: shadow, 'transition': time });
            $(".clubes img").addClass("hovered");
            //$('.clubes').css({ 'background-color': corHover, 'transition': bkgTime });
            if ($("#" + $(this).attr('id')).attr('id') === $(this).attr('id')) {
                $("#" + $(this).attr('id')).removeClass("hovered");
            }
            $(".clubes .hovered")
                .css({ 'filter': brightness50, '-webkit-filter': 'blur(25px)', 'transition': time });
        },
        function() {
            $(".clubes img").css({ filter: shadow, 'transition': time });
            $(".clubes .hovered")
                .css({ 'filter': brightness100, '-webkit-filter': 'blur(0px)', 'transition': time });
            $(".clubes img").removeClass("hovered");
            //$('.clubes').css({ 'background-color': corNormal, 'transition': bkgTime });
        });

});


//Get Info Clubs (Disabled)
$(document).ready(function() {

    $(".ClickTeste").click(function() {
        var id = $(this).attr('id');
        //getTeamList(id);
    });

    function getTeamList(id) {
        // Call Web API to get a list of teams
        $.ajax({
            url: '/api/InfoEquipasApi/' + id,
            method: 'GET',
            dataType: 'json',
            success: function(data) {
                $("#Nome").empty();
                $("#Local").empty();
                $("#Estadio").empty();
                $("#Patrocinador").empty();
                $("#NomePresidente").empty();
                $("#NomeTreinador").empty();
                $("#Plantel").empty();

                $("#Nome").text(data.nome);
                $("#Local").text(data.local);
                $("#Estadio").text(data.estadio);
                $("#Patrocinador").text(data.patrocinador);
                $("#NomePresidente").text(data.nomePresidente);
                $("#NomeTreinador").text(data.nomeTreinador);
                $("#Plantel").text(data.plantel);
            }
        });
    }

    //$(".ClickTeste").on('mouseleave', function () {

    //    $("#Nome").empty();
    //    $("#Local").empty();
    //    $("#Estadio").empty();
    //    $("#Patrocinador").empty();
    //    $("#NomePresidente").empty();
    //    $("#NomeTreinador").empty();
    //    $("#Plantel").empty();

    //});

});


//Get Data Jornadas

$(document).ready(function() {

    getAllJornada(33);

});


var ArrayAllJornadas;

var goalsHomeTeam;
var goalsAwayTeam;

function getAllJornada(matchday) {

    $.ajax({
        url: 'https://api.football-data.org/v1/competitions/457/fixtures',
        headers: {
            'X-Auth-Token': 'f947b01ed0a1434b88ce396a5ab6744a'
        },
        method: 'GET',
        dataType: 'json',
        success: function(data) {
            ArrayAllJornadas = data;
            //console.log(ArrayAllJornadas);
            try {

                $("#jornadasContent").empty();
                $("#tituloJornada").empty();
                $("#tituloJornada").text("Jornada " + matchday);


                var filteredArray = ArrayAllJornadas.fixtures.filter(function(e) {

                    return e.matchday === matchday;

                });


                $.each(filteredArray, function(i, obj) {

                    if (obj.result.goalsHomeTeam === null || obj.result.goalsAwayTeam === null) {
                        goalsHomeTeam = " ";
                        goalsAwayTeam = " ";
                    } else {
                        goalsHomeTeam = obj.result.goalsHomeTeam;
                        goalsAwayTeam = obj.result.goalsAwayTeam;
                    }

                    $("#jornadasContent").append(
                        '<div class="jogo">' +
                        '<div class="row">' +
                        '<div class="col-md-12 col-sm-12">' +
                        '<div class="team hometeam">' +
                        obj.homeTeamName +
                        '<img src="img/imgClubes/' + obj.homeTeamName + '.png">' +
                        '</div>' +

                        '<div class="marcador">' +
                        '<div class="dataJornada">' +
                        obj.date.substring(0, 10) + " - " + obj.date.substring(11, 16) +
                        '</div>' +
                        '<div class="resultados">' +
                        goalsHomeTeam + " : " + goalsAwayTeam +
                        '</div>' +
                        '</div>' +

                        '<div class="team awayteam">' +
                        '<img src="img/imgClubes/' + obj.awayTeamName + '.png">' +
                        obj.awayTeamName +
                        '</div>' +
                        '</div>' +
                        '</div>' +
                        '</div>'
                    );

                });

                console.log(filteredArray);
            } catch (e) {

                console.log("Catch - " + e);
                $("#jornadasContent").append("<h1>No data to show!</h1>");
            }


        }
    });
}


function getJornada(matchday) {

    try {

        $("#jornadasContent").empty();
        $("#tituloJornada").empty();
        $("#tituloJornada").text("Jornada " + matchday);


        var filteredArray = ArrayAllJornadas.fixtures.filter(function(e) {

            return e.matchday === matchday;

        });


        $.each(filteredArray, function(i, obj) {

            if (obj.result.goalsHomeTeam === null || obj.result.goalsAwayTeam === null) {
                goalsHomeTeam = " ";
                goalsAwayTeam = " ";
            } else {
                goalsHomeTeam = obj.result.goalsHomeTeam;
                goalsAwayTeam = obj.result.goalsAwayTeam;
            }

            $("#jornadasContent").append(
                '<div class="jogo">' +
                '<div class="row">' +
                '<div class="col-md-12 col-sm-12">' +
                '<div class="team hometeam">' +
                obj.homeTeamName +
                '<img src="img/imgClubes/' + obj.homeTeamName + '.png">' +
                '</div>' +

                '<div class="marcador">' +
                '<div class="dataJornada">' +
                obj.date.substring(0, 10) + " - " + obj.date.substring(11, 16) +
                '</div>' +
                '<div class="resultados">' +
                goalsHomeTeam + " : " + goalsAwayTeam +
                '</div>' +
                '</div>' +

                '<div class="team awayteam">' +
                '<img src="img/imgClubes/' + obj.awayTeamName + '.png">' +
                obj.awayTeamName +
                '</div>' +
                '</div>' +
                '</div>' +
                '</div>'
            );

        });

        console.log(filteredArray);
    } catch (e) {

        console.log("Catch - " + e);
        $("#jornadasContent").append("<h1>No data to show!</h1>");
    }

}




//Get Data Tabela Classificativa
$(document).ready(function() {

    setInterval(function() {
        getTableData();
    }, 300000);

    $(".refreshTable").on("click", function() {


        getTableData();


    });

    getTableData();

    function getTableData() {

        $("#ScoreTable").empty();

        $.ajax({
            url: 'https://api.football-data.org/v1/competitions/457/leagueTable',
            headers: {
                'X-Auth-Token': 'f947b01ed0a1434b88ce396a5ab6744a'
            },
            method: 'GET',
            dataType: 'json',
            success: function(data) {
                $.each(data.standing,
                    function(i, obj) {

                        $("#ScoreTable").append("<tr>");
                        $("#ScoreTable").append('<td style="padding-top: 20px">' + obj.position + "</th>");
                        $("#ScoreTable").append('<td style="text-align: center"><img class="tableImg" src="' + "img/imgClubes/" + obj.teamName + ".png" + '"</td>');
                        $("#ScoreTable").append('<td style="padding-top: 20px">' + obj.teamName + "</td>");
                        $("#ScoreTable").append('<td style="padding-top: 20px">' + obj.playedGames + "</td>");
                        $("#ScoreTable").append('<td style="padding-top: 20px">' + obj.wins + "</td>");
                        $("#ScoreTable").append('<td style="padding-top: 20px">' + obj.draws + "</td>");
                        $("#ScoreTable").append('<td style="padding-top: 20px">' + obj.losses + "</td>");
                        $("#ScoreTable").append('<td style="padding-top: 20px">' + obj.goals + "</td>");
                        $("#ScoreTable").append('<td style="padding-top: 20px">' + obj.goalsAgainst + "</td>");
                        $("#ScoreTable").append('<td style="padding-top: 20px">' + obj.points + "</td>");
                        $("#ScoreTable").append("</tr>");

                    });
                console.log(data.standing);
            }
        });
    }

});

//Carousel Jornadas
$(document).ready(function() {

    $('.owl-carousel').owlCarousel({
        loop: false,
        margin: -20,
        nav: false,
        dots: false,
        navContainer: ".navCar",
        stagePadding: 0,
        startPosition: 33,
        responsive: {
            0: {
                items: 6
            },
            425: {
                items: 10
            },
            768: {
                items: 15
            },
            992: {
                items: 20
            },
            1200: {
                items: 34
            }
        }
    });


});

//Number Jornadas Active
$(document).ready(function() {

    $('.jornadasNumbers').click(function() {

        $('.jornadasNumbers.active').removeClass('active');
        $(this).addClass('active');
    });
});