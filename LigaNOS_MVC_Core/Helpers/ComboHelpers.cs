﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using LigaNOS_MVC_Core.Data;
using LigaNOS_MVC_Core.Models;

namespace LigaNOS_MVC_Core.Helpers
{
    public class ComboHelpers
    {

        private readonly LigaNosDbContext _context;

        public ComboHelpers(LigaNosDbContext context)
        {
            _context = context;
        }

        public List<Equipa> GetEquipas()
        {
            var Equipas = _context.Equipa.ToList();


            return Equipas;
        }

        public List<string> GetNomesEquipas()
        {
            var Equipas = _context.Equipa.ToList();

            var NomesEquipas = new List<string>();

            foreach (var item in Equipas)
            {
                NomesEquipas.Add(item.Nome);
            }

            return NomesEquipas;
        }


        public List<int> GetNumeroDeEquipas()
        {
            var CountEquipas = _context.Equipa.Count();
            var NumeroDeEquipas = _context.Equipa.ToList();

            int[] n = new int[CountEquipas];

            int i = 0;
            int nu = 1;

            foreach (var item in NumeroDeEquipas)
            {
                n[i] = nu;

                i++;
                nu++;
            }

            return n.ToList();
        }


    }
}
